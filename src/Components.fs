namespace App

open Feliz
open Fable.Core.JsInterop

type Components =
    [<ReactComponent>]
    static member Button (props: {| sound: Sounds.SoundObject |}) =
        Html.button [
            prop.onClick (fun _ ->
                Sounds.playOrStopSound props.sound
            )
            prop.children [
                Html.img [
                    prop.src props.sound.ImageFile
                ]
            ]
        ]

    static member Switch (props: {| value: bool; onPress: bool -> unit; onColor: string; offColor: string |}) =
        importAll "./styles/switch.sass"
        let (value, setValue) = React.useState(false)
        printfn "value: %b" value
        Html.button [
            prop.onClick (fun _ ->
                setValue (not value)
                props.onPress (not value)
            )
            prop.className "switch"
            prop.children [
                Html.div [
                    prop.classes [
                        "ball"
                        if value = true then "switch-on"
                        else "switch-off"
                    ]

                    prop.style [
                        style.backgroundColor (
                            if value = true then props.onColor
                            else props.offColor
                        )
                    ]
                ]
            ]
        ]
