const path = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const Dotenv = require('dotenv-webpack')

// export the configuration as an object
module.exports = {
	// development mode will set some useful defaults in webpack
	mode: "development",
	// the entry point is the top of the tree of modules.
	// webpack will bundle this file and everything it references.
	entry: "./src/Main.fs.js",
	// we specify we want to put the bundled result in the matching out/ folder
	output: {
		filename: "index.js",
		path: path.resolve(__dirname, "out/"),
	},
	plugins: [
		new Dotenv,
		new HtmlWebpackPlugin({
			template: "src/index.html",
		}),
	],
	module: {
		// rules tell webpack how to handle certain types of files
		rules: [
			{
				test: /\.(sass|scss|css)$/,
				use: [
					"style-loader",
					{
						loader: "css-loader",
					},
					{
						loader: "sass-loader",
						options: { implementation: require("sass") }
					}
				],
			},
			{
				test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)(\?.*)?$/,
				use: [ "file-loader" ]
			}
		],
	},
	resolve: {
		// specify certain file extensions to get automatically appended to imports
		// ie we can write `import "index"` instead of `import "index.ts"`
		extensions: [ ".js" ],
	}
}